from django.conf.urls import include
from django.urls import path

from .views import MembershipApplicationAddView, MembershipApplicationDetailView, MembershipApplicationListView
from .views import IndividualMembershipApplicationAddView

app_name = 'application'
urlpatterns = [
    path('view/<str:view_link_key>/', MembershipApplicationDetailView.as_view(lookup_field='view_link_key'),
        name='application-view'),
    path('list/', MembershipApplicationListView.as_view(), name='application-list'),
    path('', MembershipApplicationAddView.as_view(), name='application-add'),
    path('individual/', IndividualMembershipApplicationAddView.as_view(), name='individual-application-add'),
]
