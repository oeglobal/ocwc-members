# -*- coding: utf-8 -*-
import logging
from django.core.management.base import BaseCommand, CommandError
from crm.models import Organization, BillingLog, Profile, Contact
from quickbooks.objects.invoice import Invoice as QuickBooksInvoice
from quickbooks.objects.detailline import SalesItemLine, SalesItemLineDetail
from quickbooks.objects.item import Item
from quickbooks.exceptions import QuickbooksException, AuthorizationException
from quickbooks.objects.base import EmailAddress, Ref
from django.contrib.auth.models import User
import datetime
from django.conf import settings
from quickbooks.helpers import qb_date_format

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    """
    Management command to create a draft invoice in Quickbooks for a member organization.
    This command only creates the invoice - it does not send it to the customer.
    To send the invoice, use the Quickbooks UI or a separate command.

    Before using this command:
    1. Ensure Quickbooks credentials are set in settings.py (QB_CLIENT_ID, QB_CLIENT_SECRET)
    2. Run sync_qbo command first to ensure valid Quickbooks authorization
    3. Make sure the organization has a valid qbo_id
    4. Ensure the required settings are defined in settings.py

    Takes two required parameters:
    - member_id: The Organization ID to bill
    - amount: The amount to bill in dollars

    Example usage:
    python manage.py create_invoice 123 500
    """

    help = "Create a draft Quickbooks invoice for the specified member and amount"

    def add_arguments(self, parser):
        parser.add_argument("member_id", type=int, help="Organization ID to bill")
        parser.add_argument("amount", type=float, help="Amount to bill in dollars")

    def get_next_available_doc_number(self, qb_client, base_doc_number):
        """
        Find the next available document number by checking if the base number exists
        and appending a sequence number if needed.
        """
        try:
            # Try to query for invoices with the base doc number
            invoice_list = QuickBooksInvoice.filter(
                qb=qb_client,
                DocNumber=base_doc_number,
            )

            if not invoice_list:
                return base_doc_number

            # If base number exists, try with sequence numbers
            sequence = 1
            while True:
                doc_number = f"{base_doc_number}-{sequence}"
                invoice_list = QuickBooksInvoice.filter(
                    qb=qb_client,
                    DocNumber=doc_number,
                )
                if not invoice_list:
                    return doc_number
                sequence += 1

        except QuickbooksException:
            # If there's an error querying, just return the base number
            # and let the main error handling deal with any issues
            return base_doc_number

    def handle(self, *args, **options):
        member_id = options["member_id"]
        amount = options["amount"]

        try:
            # Get the organization
            org = Organization.objects.get(id=member_id)
            if not org.qbo_id:
                raise CommandError(
                    f"Organization {member_id} does not have a Quickbooks ID"
                )

            # Get QB client
            qb_client, profile = Profile.get_qb_client()
            if not qb_client:
                raise CommandError("Could not initialize Quickbooks client")

            # Check if invoice already exists
            doc_number = f"{member_id}-{settings.INVOICE_YEAR}"
            existing_invoices = QuickBooksInvoice.filter(
                qb=qb_client,
                DocNumber=doc_number,
            )
            if existing_invoices:
                self.stdout.write(
                    self.style.WARNING(
                        f'Invoice #{doc_number} already exists for organization "{org.display_name}". Skipping creation.'
                    )
                )
                return

            # Create the invoice line
            line = SalesItemLine()
            line.Amount = amount
            line.Description = settings.INVOICE_DESCRIPTION

            # Set up the line details
            detail = SalesItemLineDetail()
            detail.Qty = 1
            detail.UnitPrice = amount

            # Get item with ID 3
            item = Item.get(3, qb=qb_client)
            detail.ItemRef = item.to_ref()

            line.SalesItemLineDetail = detail

            # Create the draft invoice
            invoice = QuickBooksInvoice()
            invoice.CustomerRef = Ref()
            invoice.CustomerRef.value = str(org.qbo_id)  # Convert to string to be safe
            invoice.Line = [line]
            invoice.DocNumber = doc_number

            # Set invoice and due dates to current date
            today = datetime.date.today()
            invoice.TxnDate = today.strftime("%Y-%m-%d")
            invoice.DueDate = today.strftime("%Y-%m-%d")

            # Add customer message and payment details
            # invoice.CustomerMemo = settings.INVOICE_PAYMENT_MESSAGE
            invoice.PrivateNote = settings.INVOICE_PAYMENT_DETAILS

            # Get billing contacts
            billing_contacts = Contact.objects.filter(
                organization=org, contact_type=13
            )  # 13 is Accounting Contact
            if not billing_contacts:
                # Fallback to lead contact if no billing contact
                billing_contacts = Contact.objects.filter(
                    organization=org, contact_type=6
                )  # 6 is Lead Contact

            if not billing_contacts:
                raise CommandError(
                    f"No billing or lead contact found for organization {member_id}"
                )

            # Set billing email
            primary_email = EmailAddress()
            primary_email.Address = billing_contacts[0].email
            invoice.BillEmail = primary_email

            # Set CC emails
            cc_emails = [
                c.email for c in billing_contacts[1:]
            ]  # Additional billing contacts
            cc_emails.extend(settings.INVOICE_CC_EMAILS.split(", "))
            if cc_emails:
                cc_email = EmailAddress()
                cc_email.Address = ", ".join(cc_emails)
                invoice.BillEmailCc = cc_email

            # Save as draft
            result = invoice.save(qb=qb_client)

            # Log the draft invoice creation
            user = User.objects.filter(is_staff=True).first()
            if not user:
                raise CommandError("No staff user found to associate with billing log")

            BillingLog.objects.create(
                organization=org,
                log_type="create_invoice",
                user=user,
                amount=amount,
                qbo_id=result.Id,
                created_date=datetime.date.today(),
                description=settings.INVOICE_DESCRIPTION,
                invoice_number=doc_number,
                invoice_year=settings.INVOICE_YEAR,
            )

            self.stdout.write(
                self.style.SUCCESS(
                    f'Successfully created draft invoice {result.Id} (#{doc_number}) for organization "{org.display_name}" for ${amount}'
                )
            )

        except Organization.DoesNotExist:
            raise CommandError(f"Organization {member_id} does not exist")

        except QuickbooksException as e:
            logger.error(f"Quickbooks error creating draft invoice: {str(e)}")
            raise CommandError(f"Quickbooks error: {str(e)}")

        except Exception as e:
            logger.error(f"Error creating draft invoice: {str(e)}")
            raise CommandError(f"Error creating draft invoice: {str(e)}")
