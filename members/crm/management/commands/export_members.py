# -*- coding: utf-8 -*-
from __future__ import print_function

import csv

from django.conf import settings
from django.core.management.base import BaseCommand

from crm.models import Contact, Organization


class Command(BaseCommand):
    """
    Some implementation notes:

    - adjusted copy of `OrganizationExportExcel`
    - main intended use-case: export of stuff for later import into Tendency
    """

    help = "exports active members and their contacts as set of CSV files"

    def add_arguments(self, parser):
        parser.add_argument(
            "csv_file_name_prefix",
            help="prefix for names of the CSV files to export to",
        )

    def _get_contact(self, obj):
        contact = None
        try:
            contact = obj.contact_set.filter(contact_type=6)[0]
        except IndexError:
            pass  # ignored, None returned
        return contact

    def _export_users(self, file_name, is_individual):
        with open(file_name, "w", newline="") as csvfile:
            csvwriter = csv.writer(csvfile)
            csvwriter.writerow(
                [
                    "member_number",
                    "QBO id",
                    "username",
                    "membership_type",
                    "status",
                    "join_dt",
                    "email",
                    "first_name",
                    "last_name",
                    "position_title",
                    "address",
                    "address2",
                    "city",
                    "zipcode",
                    "state",
                    "country",
                ]
            )
            counter = 0
            for obj in Organization.objects.filter(
                membership_status__in=(2, 3, 5, 7), individual_member=is_individual
            ).order_by("id"):
                # if there's no country (which is required), skip this record
                # otherwise this will crash: https://members.oeglobal.org/staff/organization/list/excel/
                try:
                    c = obj.address_set.first().country
                except:
                    print("WARNING: skipping record (missing country): " + str(obj))
                    continue
                contact = self._get_contact(obj)
                if contact is None:
                    print("WARNING: skipping record (missing contact): " + str(obj))
                    continue
                address = obj.address_set.first()

                csvwriter.writerow(
                    [
                        obj.pk,
                        obj.qbo_id,
                        obj.user.username,
                        "Individual"
                        if is_individual
                        else obj.get_membership_type_display(),
                        "Active" if obj.membership_status != 5 else "Admin Hold",
                        obj.created,
                        contact.email,
                        contact.first_name,
                        contact.last_name,
                        contact.job_title,
                        address.street_address,
                        "%s, %s"
                        % (
                            address.supplemental_address_1,
                            address.supplemental_address_2,
                        )
                        if address.supplemental_address_2
                        else address.supplemental_address_1,
                        address.city,
                        "%s %s" % (address.postal_code, address.postal_code_suffix)
                        if address.postal_code_suffix
                        else address.postal_code,
                        address.state_province,
                        address.country.name if address.country else None,
                    ]
                )

                counter += 1
        print("%d individual members exported into %s" % (counter, file_name))

    def handle(self, *args, **options):
        # individual users
        file_name = "%s%s" % (options["csv_file_name_prefix"], "members-individual.csv")
        self._export_users(file_name, True)

        # institutions
        # TODO

        # users which are institution contacts
        # TODO:
        # - member_number defines institution, hence should NOT be used for users here
        # - mapping of user to institution
        # - membership types: check whether they DO exists in Tendenci (if not, create)
        file_name = "%s%s" % (
            options["csv_file_name_prefix"],
            "members-institutional.csv",
        )
        self._export_users(file_name, False)
