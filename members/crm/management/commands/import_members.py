import csv
import sys

from django.core.management.base import BaseCommand, CommandError
from django.db import transaction

from crm.models import Country, MembershipApplication


class Command(BaseCommand):
    """
    Some implementation notes:

    - if column name does not match any field in the MembershipApplication model, it is ignored
    - if some fields are missing, some hopefully sane defaults are filled in
    - if no sane default is possible, import fails and missing stuff needs to be added into CSV
    - this was done at a time of this CRM being phased-out, hence effort and testing put into this was low => BEWARE OF DRAGONS
    """

    help = "Import members from CSV file"

    def add_arguments(self, parser):
        parser.add_argument(
            "csv_file",
            help="CSV file with members to import, first line a header matching motel field names, UTF-8 encoded",
        )

    def get_column_index(self, name):
        if name not in self.csv_header:
            return -1
        return self.csv_header.index(name)

    def set_csv_header(self, csv_header):
        self.csv_header = csv_header
        if (
            self.get_column_index("first_name") < 0
            or self.get_column_index("last_name") < 0
            or self.get_column_index("email") < 0
        ):
            print("CSV needs to have at least first_name, last_name and email columns")
            sys.exit(-1)

    def get(self, row, name, default=None):
        if name not in self.csv_header:
            return default
        return row[self.get_column_index(name)].strip()

    def get_country(self, row):
        country_name = self.get(row, "country")
        if not country_name:
            return None
        country = None
        try:
            country = Country.objects.get(name__iexact=country_name)
        except Country.DoesNotExist:
            print(
                "WARNING: unknown country: %s -> will leave this field blank"
                % country_name
            )
        return country

    def get_individual_member(self, row):
        return self.get(row, "individual_member", "false").lower() in (
            "yes",
            "true",
            "t",
            "1",
        )

    def get_membership_type(self, row):
        return int(self.get(row, "membership_type", "5"))

    @transaction.atomic
    def handle(self, *args, **options):
        count_processed = 0
        count_imported = 0
        with open(options["csv_file"], newline="", encoding="utf-8") as csv_file:
            csv_reader = csv.reader(csv_file)

            csv_header = next(csv_reader)
            self.set_csv_header(csv_header)

            for row in csv_reader:
                first_name = self.get(row, "first_name")
                last_name = self.get(row, "last_name")
                email = self.get(row, "email")
                count_processed += 1

                count = MembershipApplication.objects.filter(
                    first_name=first_name, last_name=last_name, email=email
                ).count()
                if count > 0:
                    print(
                        "WARNING: membership application for '%s %s <%s>' already exists -> skipping"
                        % (first_name, last_name, email)
                    )
                    continue

                ma = MembershipApplication()
                ma.first_name = first_name
                ma.last_name = last_name
                ma.email = email
                ma.individual_member = self.get_individual_member(row)
                ma.display_name = self.get(row, "display_name")
                ma.country = self.get_country(row)
                ma.membership_type = self.get_membership_type(row)

                # agreements: it is assumed that wherever members applied, terms were there and were agreed
                ma.moa_terms = True
                ma.terms_of_use = True
                ma.coppa = True

                # approval: it is assumed that whoever is importing members, is importing only approved ones
                ma.app_status = "Approved"

                ma.save(email_notification=False)
                print("member '%s %s <%s> imported'" % (first_name, last_name, email))
                count_imported += 1

        print("%d entries processed, %d imported" % (count_processed, count_imported))
