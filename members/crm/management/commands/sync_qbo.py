# -*- coding: utf-8 -*-
import logging
from django.core.management.base import BaseCommand, CommandError
from quickbooks.objects.invoice import Invoice as QuickBooksInvoice
from quickbooks.objects.payment import Payment as QuickBooksPayment
from quickbooks.exceptions import AuthorizationException, QuickbooksException
from quickbooks import Oauth2SessionManager
import arrow
import webbrowser
import datetime

from crm.models import Profile, Organization, Invoice, BillingLog
from django.contrib.auth.models import User
from django.conf import settings

logger = logging.getLogger(__name__)

class Command(BaseCommand):
    help = "Initialize Quickbooks connection and download information from QBO"

    def add_arguments(self, parser):
        parser.add_argument(
            "--auth-code",
            help="Authorization code from Quickbooks OAuth flow",
            required=False,
        )
        parser.add_argument(
            "--realmid",
            help="Realm ID from Quickbooks OAuth flow",
            required=False,
        )
        parser.add_argument(
            "--force-auth",
            action="store_true",
            help="Force new authorization even if existing connection is valid",
        )

    def handle(self, *args, **options):
        # Verify QB settings are configured
        if not all([settings.QB_CLIENT_ID, settings.QB_CLIENT_SECRET, settings.QB_CALLBACK_URL]):
            raise CommandError(
                "Quickbooks credentials not configured. Please set QB_CLIENT_ID, QB_CLIENT_SECRET and QB_CALLBACK_URL in settings."
            )

        # Check for existing valid connection unless force-auth is specified
        if not options.get("force_auth"):
            # Get staff user profile
            user = User.objects.filter(is_staff=True).first()
            if not user:
                raise CommandError("No staff user found")

            profile = user.profile

            # Debug info
            self.stdout.write(f"Profile token status:")
            self.stdout.write(f"- Valid: {profile.qb_valid}")
            self.stdout.write(f"- Has access token: {bool(profile.qb_access_token)}")
            self.stdout.write(f"- Has refresh token: {bool(profile.qb_refresh_token)}")
            self.stdout.write(f"- Token expires: {profile.qb_token_expires}")
            self.stdout.write(f"- Refresh expires: {profile.qb_refresh_expires}")

            # Check if we have valid tokens
            if (profile.qb_valid and profile.qb_access_token and profile.qb_refresh_token and
                profile.qb_token_expires and profile.qb_token_expires > datetime.datetime.now()):
                self.stdout.write(self.style.SUCCESS("Using existing valid Quickbooks connection"))
                try:
                    self.get_qbo_events()
                    return
                except QuickbooksException as e:
                    self.stdout.write(self.style.WARNING(f"Error with existing connection: {str(e)}"))
                    self.stdout.write(self.style.WARNING("Will try to refresh token..."))
                    try:
                        session_manager = Oauth2SessionManager(
                            client_id=settings.QB_CLIENT_ID,
                            client_secret=settings.QB_CLIENT_SECRET,
                            base_url=settings.QB_CALLBACK_URL,
                            refresh_token=profile.qb_refresh_token,
                        )
                        session_manager.refresh_access_tokens()
                        profile.qb_access_token = session_manager.access_token
                        profile.qb_refresh_token = session_manager.refresh_token
                        profile.qb_token_expires = arrow.now().shift(seconds=3600).datetime
                        profile.save()
                        self.stdout.write(self.style.SUCCESS("Successfully refreshed token"))
                        self.get_qbo_events()
                        return
                    except Exception as e:
                        self.stdout.write(self.style.ERROR(f"Error refreshing token: {str(e)}"))
                        self.stdout.write(self.style.WARNING("Will try to reauthorize..."))
            else:
                self.stdout.write(self.style.WARNING("Existing connection is not valid or tokens are expired"))

        # Start or complete OAuth flow
        if not options.get("auth_code"):
            # Start OAuth flow
            auth_url = self.get_auth_url()
            self.stdout.write(
                self.style.WARNING(
                    f"\nNo auth code provided. Please visit this URL to authorize:\n{auth_url}\n"
                    "\nAfter authorizing, copy the authorization code and realm ID from the URL"
                    "\nand run this command again with:"
                    "\npython manage.py sync_qbo --auth-code=<code> --realmid=<realmid>"
                )
            )
            try:
                webbrowser.open(auth_url)
            except:
                pass
            return

        # Complete OAuth flow with provided code
        try:
            self.initialize_qb_session(options["auth_code"], options["realmid"])
            self.get_qbo_events()
        except QuickbooksException as e:
            self.stdout.write(self.style.ERROR(f"Quickbooks error: {str(e)}"))
            return

    def get_auth_url(self):
        """Get the Quickbooks authorization URL"""
        session_manager = Oauth2SessionManager(
            client_id=settings.QB_CLIENT_ID,
            client_secret=settings.QB_CLIENT_SECRET,
            base_url=settings.QB_CALLBACK_URL,
        )
        return session_manager.get_authorize_url(settings.QB_CALLBACK_URL)

    def initialize_qb_session(self, auth_code, realm_id):
        """Initialize Quickbooks session with OAuth code"""
        # Get or create staff user profile
        user = User.objects.filter(is_staff=True).first()
        if not user:
            raise CommandError("No staff user found")

        profile = user.profile
        profile.update_qb_session_manager(auth_code, realm_id)
        self.stdout.write(self.style.SUCCESS("Successfully initialized Quickbooks session"))

    def get_qbo_events(self):
        """Download invoice and payment data from Quickbooks"""
        qb_client, profile = Profile.get_qb_client()
        if not qb_client:
            raise QuickbooksException("Could not initialize Quickbooks client")

        user = User.objects.filter(is_staff=True).first()
        if not user:
            raise QuickbooksException("No staff user found")

        self.stdout.write("Syncing invoices...")
        for offset in [1, 100, 200, 300, 400, 500, 600, 700]:
            self.stdout.write(f"Processing offset {offset}...")
            try:
                invoices = QuickBooksInvoice.all(qb=qb_client, start_position=offset)
            except AuthorizationException:
                profile.qb_valid = False
                profile.save()
                raise QuickbooksException("Authorization expired. Please reauthorize.")

            for qb_invoice in invoices:
                try:
                    org = Organization.objects.get(qbo_id=qb_invoice.CustomerRef.value)
                except Organization.DoesNotExist:
                    continue

                log, is_created = BillingLog.objects.get_or_create(
                    qbo_id=qb_invoice.Id,
                    log_type="create_invoice",
                    defaults={
                        "organization": org,
                        "user": user,
                        "amount": qb_invoice.TotalAmt,
                    },
                )

                if is_created:
                    log.pub_date = arrow.get(
                        qb_invoice.MetaData["LastUpdatedTime"]
                    ).datetime
                    log.created_date = arrow.get(
                        qb_invoice.MetaData["CreateTime"]
                    ).date()
                    log.save()
                else:
                    log.invoice_year = log.pub_date.year
                    log.created_date = arrow.get(
                        qb_invoice.MetaData["CreateTime"]
                    ).date()
                    log.save()

        self.stdout.write("Syncing payments...")
        for offset in [1, 100, 200, 300, 400, 500, 600, 700]:
            self.stdout.write(f"Processing offset {offset}...")
            payments = QuickBooksPayment.all(qb=qb_client, start_position=offset)
            for qb_payment in payments:
                try:
                    org = Organization.objects.get(qbo_id=qb_payment.CustomerRef.value)
                except Organization.DoesNotExist:
                    continue

                log, is_created = BillingLog.objects.get_or_create(
                    qbo_id=qb_payment.Id,
                    log_type="create_payment",
                    defaults={
                        "organization": org,
                        "user": user,
                        "amount": qb_payment.TotalAmt,
                    },
                )

                if is_created:
                    log.pub_date = arrow.get(
                        qb_payment.MetaData["LastUpdatedTime"]
                    ).datetime
                    log.save()

        self.stdout.write(self.style.SUCCESS("Successfully synced Quickbooks data"))
