# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations

OLD_IM_FLAG = '(Individual Member)'


def forwards(apps, schema_editor):
    Organization = apps.get_model('crm', 'Organization')
    individual_members = Organization.objects.filter(display_name__contains=OLD_IM_FLAG)

    for individual_member in individual_members:
        individual_member.individual_member = True
        old_dn = individual_member.display_name
        new_dn = old_dn[:old_dn.index(OLD_IM_FLAG)].strip()
        individual_member.display_name = new_dn
        individual_member.save()
        print('Org. "{}" renamed to "{}" and flagged as individual member.'.format(old_dn, new_dn))


class Migration(migrations.Migration):
    dependencies = [('crm', '0032_organization_individual_member')]

    operations = [migrations.RunPython(forwards, hints={'target_db': 'default'})]
