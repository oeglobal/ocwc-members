# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def forwards(apps, schema_editor):
    Organization = apps.get_model("crm", "Organization")
    individual_members = Organization.objects.filter(
        created__date="2023-03-22", individual_member=True, display_name__startswith="@"
    )

    for individual_member in individual_members:
        old_dn = individual_member.display_name
        first_contact = individual_member.contact_set.first()
        new_dn = first_contact.first_name + " " + first_contact.last_name
        individual_member.display_name = new_dn
        individual_member.save()
        print('Org. "{}" renamed to "{}".'.format(old_dn, new_dn))


class Migration(migrations.Migration):
    dependencies = [("crm", "0038_auto_20230212_0301")]

    operations = [migrations.RunPython(forwards, hints={"target_db": "default"})]
