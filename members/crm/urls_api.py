from django.urls import path

from .views import (
    address_geo_list_view,
    country_list_view,
    OrganizationByCountryListViewApi,
    organization_group_by_membership_view,
    OrganizationViewApi,
    OrganizationRssFeedsApi,
    organization_group_by_consortium_view,
    OrganizationListViewApi,
)

app_name = "crmapi"
urlpatterns = [
    path("address/list/geo/", address_geo_list_view, name="address-list-geo"),
    path(
        "address/list/geo/consortium/<str:consortium>/",
        address_geo_list_view,
        name="address-list-geo",
    ),
    path("country/list/", country_list_view, name="country-list"),
    path(
        "organization/view/<int:pk>/",
        OrganizationViewApi.as_view(),
        name="organization-view",
    ),
    path(
        "organization/list/",
        OrganizationListViewApi.as_view(),
        name="organization-by_country-list",
    ),
    path(
        "organization/by_country/<str:country>/list/",
        OrganizationByCountryListViewApi.as_view(lookup_field="country"),
        name="organization-by_country-list",
    ),
    path(
        "organization/group_by/membership_type/list/",
        organization_group_by_membership_view,
        name="organization-group_by_membership_type-list",
    ),
    path(
        "organization/group_by/consortium/<str:consortium>/list/",
        organization_group_by_consortium_view,
        name="organization-group_by_membership_type-list",
    ),
    path(
        "organization/feeds/",
        OrganizationRssFeedsApi.as_view(),
        name="organization-feeds",
    ),
]
