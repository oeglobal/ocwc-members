# -*- coding: utf-8 -*-
import datetime
from dateutil.relativedelta import relativedelta

from django import forms
from django.utils.safestring import mark_safe

from captcha.fields import ReCaptchaField
from captcha.widgets import ReCaptchaV3

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Field, Div, HTML

from .models import (
    MembershipApplication,
    ORGANIZATION_ASSOCIATED_CONSORTIUM,
    Organization,
    Address,
    BillingLog,
    BILLING_LOG_TYPE_CHOICES,
    ReportedStatistic,
)

SIMPLIFIED_MEMBERSHIP_TYPE_CHOICES = (
    (
        "institutional",
        mark_safe(
            'Institutional Member <i class="icon-question-sign" data-help-text="institutional"></i>'
        ),
    ),
    (
        "associate",
        mark_safe('Associate Consortium Member <i class="icon-question-sign"></i>'),
    ),
    (
        "organizational",
        mark_safe('Organizational Member <i class="icon-question-sign"></i>'),
    ),
    ("corporate", mark_safe('Corporate Member <i class="icon-question-sign"></i>')),
)

ORGANIZATION_ASSOCIATED_CONSORTIUM_CHOICES = filter(
    lambda x: x[0] not in ["CORE", "KOCWC"], ORGANIZATION_ASSOCIATED_CONSORTIUM
)


class MembershipApplicationModelForm(forms.ModelForm):
    associate_consortium = forms.ChoiceField(
        choices=(("", "---------"),)
        + tuple(ORGANIZATION_ASSOCIATED_CONSORTIUM_CHOICES),
        required=False,
    )

    moa_terms = forms.BooleanField(required=True, label="I agree to these terms")

    terms_of_use = forms.BooleanField(
        required=True, label="I agree to the terms of use for this website."
    )
    coppa = forms.BooleanField(
        required=True, label="I signify that I am 13 years of age or older."
    )

    main_website = forms.URLField(
        required=True, label="Main Website address (with http:// or https:// in front)"
    )

    initiative_url1 = forms.URLField(required=False, label="URL")
    initiative_url2 = forms.URLField(required=False, label="URL")
    initiative_url3 = forms.URLField(required=False, label="URL")
    captcha = ReCaptchaField()

    def __init__(self, *args, **kwargs):
        super(MembershipApplicationModelForm, self).__init__(*args, **kwargs)

        self.fields[
            "description"
        ].label = "Institution Description (500 - 1200 characters)"
        self.fields["support_commitment"].label = ""
        self.fields[
            "accreditation_body"
        ].help_text = "If your organization is accredited, please provide the name of the accreditation body here."
        self.fields["country"].help_text = mark_safe(
            "Select the country/region in which the institution is located. This will be used for grouping in the members display area on the website."
        )
        self.fields["country"].label = "Country/region"

        self.helper = FormHelper(self)
        self.helper.form_show_errors = True

        self.helper.layout = Layout(
            Div(
                HTML(
                    '<div class="large-8 columns"><h3>Lead Contact Person</h3><p>This is the person who will act as lead contact for communications from OEGlobal</p></div>'
                ),
                Div(
                    Div(
                        Field("first_name", required=True),
                        css_class="large-6 columns field-collapse text-full-width",
                    ),
                    Div(
                        Field("last_name", required=True),
                        css_class="large-6 columns field-collapse text-full-width",
                    ),
                    css_class="row",
                ),
                Field("job_title", required=True),
                Field("email", required=True),
                css_class="row",
            ),
            Div(
                HTML(
                    '<div class="large-8 columns"><h3>About Your Organization</h3></div>'
                ),
                Field("display_name", required=True),
                Field("description", required=True),
                Field("logo_large"),
                # Field('is_accredited', required=True),
                # 'accreditation_body',
                Field("main_website", required=True, placeholder="http://"),
                Field("institution_type", required=True),
                Field("associate_consortium"),
                # 'country',
                css_class="row",
            ),
            Div(
                HTML('<div class="large-8 columns"><h3>Address</h3></div>'),
                Field("street_address", required=True),
                "supplemental_address_1",
                "supplemental_address_2",
                Div(
                    Div(
                        Field("city", required=True),
                        css_class="large-6 columns field-collapse text-full-width",
                    ),
                    Div(
                        Field("postal_code", required=True),
                        css_class="large-6 columns field-collapse",
                    ),
                    css_class="row",
                ),
                "state_province",
                Field("country", required=True),
                css_class="row",
            ),
            Div(
                HTML(
                    '<div class="large-8 columns"><h3>Open Education Engagement</h3></div>'
                ),
                HTML(
                    '<div class="large-8 columns"><p>Please describe your motivation for joining OE Global. (1000-1500 characters)</p></div>'
                ),
                Field("support_commitment", required=True),
                css_class="row",
            ),
            Div(
                HTML(
                    '<div class="large-8 columns"><h3>Institutional Initiatives in Open Education</h3></div>'
                ),
                HTML(
                    '<div class="large-8 columns"><p>Please share  the name, description, and web addresses '
                    "for 1 to 3 open education initiatives in place or in planning at your institution "
                    "(e.g.. Open Pedagogy, Open Textbooks, OER Development, Open Courses, Open Research, "
                    "etc).</p></div>"
                ),
                HTML('<div class="large-8 columns"><h4>Open Initiative 1</h4></div>'),
                Field("initiative_title1"),
                Field("initiative_description1"),
                Field("initiative_url1"),
                HTML('<div class="large-8 columns"><h4>Open Initiative 2</h4></div>'),
                Field("initiative_title2"),
                Field("initiative_description2"),
                Field("initiative_url2"),
                HTML('<div class="large-8 columns"><h4>Open Initiative 3</h4></div>'),
                Field("initiative_title3"),
                Field("initiative_description3"),
                Field("initiative_url3"),
                css_class="row",
            ),
            Div(
                HTML(
                    '<div class="large-8 columns"><h3>Memorandum of Association</h3></div>'
                ),
                HTML('<div class="moa-text large-8 columns"></div>'),
                Field("moa_terms", required=True),
                HTML(
                    '<div class="large-8 columns"><h3>Website Terms and Conditions</h3></div>'
                ),
                HTML('<div class="terms-text large-8 columns"></div>'),
                Field("terms_of_use", required=True),
                HTML(
                    '<div class="large-8 columns"><h3>Children\'s Online '
                    "Privacy Protection Act Compliance</h3></div>"
                ),
                HTML('<div class="coppa-text large-8 columns"></div>'),
                Field("coppa", required=True),
                Field("captcha", required=True),
                css_class="row",
            ),
        )
        self.helper.layout.append(Submit("save", "Submit Application"))

    def clean(self):
        cleaned_data = super(MembershipApplicationModelForm, self).clean()
        cleaned_data["individual_member"] = False
        return cleaned_data

    class Meta:
        model = MembershipApplication
        exclude = ("id",)


class IndividualMembershipApplicationModelForm(forms.ModelForm):
    moa_terms = forms.BooleanField(required=True, label="I agree to these terms")

    terms_of_use = forms.BooleanField(
        required=True, label="I agree to the terms of use for this website."
    )
    coppa = forms.BooleanField(
        required=True, label="I signify that I am 13 years of age or older."
    )

    job_title = forms.CharField(
        label="Role As an Open Educator",
        help_text="Job title or brief description of your current work",
    )

    description = forms.CharField(
        widget=forms.Textarea,
        label="Brief Biography",
        help_text="Enter a bio (~500 word or less) that include elements that demonstrate your efforts in advancing open education."
        "This will be the basis for your membership bio that can be edited in the future.",
    )

    main_website = forms.URLField(
        label="Personal Web Site",
        help_text="Include link to web site or professional profile (e.g. LinkedIn)",
    )

    display_name = forms.CharField(
        label="OEG Connect Username",
        help_text="All members are invited to join our online community. If you do not have an account in OEG Connect,"
        'please create one at <a href="https://connect.oeglobal.org/signup">https://connect.oeglobal.org/signup</a>.',
    )

    initiative_url1 = forms.URLField(required=False, label="URL")
    initiative_url2 = forms.URLField(required=False, label="URL")
    initiative_url3 = forms.URLField(required=False, label="URL")
    captcha = ReCaptchaField()

    def __init__(self, *args, **kwargs):
        super(IndividualMembershipApplicationModelForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper(self)
        self.helper.form_show_errors = True

        self.helper.layout = Layout(
            Div(
                HTML('<div class="large-8 columns"><h3>About You</h3></div>'),
                Div(
                    Div(
                        Field("first_name", required=True),
                        css_class="large-6 columns field-collapse text-full-width",
                    ),
                    Div(
                        Field("last_name", required=True),
                        css_class="large-6 columns field-collapse text-full-width",
                    ),
                    css_class="row",
                ),
                Field("email", required=True),
                Field("org_affiliation"),
                Field("job_title", required=True),
                Field("description", required=True),
                Field("main_website", placeholder="http://"),
                Field(
                    "display_name"
                ),  # TODO: required? right now yes, due to model; but "individual app. BETA" is NOT
                css_class="row",
            ),
            Div(
                HTML(
                    '<div class="large-8 columns"><h3>Postal Address</h3>'
                    "<p>Mailing address for billing purposes only.</p></div>"
                ),
                Field("street_address", required=True),
                Div(
                    Div(
                        Field("city", required=True),
                        css_class="large-6 columns field-collapse text-full-width",
                    ),
                    Div(
                        Field("postal_code", required=True),
                        css_class="large-6 columns field-collapse",
                    ),
                    css_class="row",
                ),
                "state_province",
                Field("country", required=True),
                css_class="row",
            ),
            Div(
                HTML(
                    '<div class="large-8 columns"><h3>Commitment and Interests</h3></div>'
                ),
                HTML(
                    '<div class="large-8 columns"><p>Here we ask for a statement to assert your motivation for joining our Organization and information on up to 3 relevant projects/initiatives.'
                    "This information will be used for both the application review process and once approved, will be part of your individual member profile (you will able to edit, change at anytime).</p></div>"
                ),
                HTML(
                    '<div class="large-8 columns"><h4>Open Education Engagement</h4>'
                    "<p>Please describe your motivation for joining OE Global. (1000-1500 characters)</p></div>"
                ),
                Field("support_commitment", required=True),
                HTML(
                    '<div class="large-8 columns"><h4>Open Education Initiatives  / Interests</h4>'
                    "<p>To help us understand better your work in the field, please provide up to three examples of projects or initiatives that you have been / are involved with.</p></div>"
                ),
                HTML('<div class="large-8 columns"><h5>Open Initiative 1</h5></div>'),
                Field("initiative_title1", required=True),
                Field("initiative_description1", required=True),
                Field("initiative_url1", required=True),
                HTML('<div class="large-8 columns"><h5>Open Initiative 2</h5></div>'),
                Field("initiative_title2"),
                Field("initiative_description2"),
                Field("initiative_url2"),
                HTML('<div class="large-8 columns"><h5>Open Initiative 3</h5></div>'),
                Field("initiative_title3"),
                Field("initiative_description3"),
                Field("initiative_url3"),
                css_class="row",
            ),
            Div(
                HTML(
                    '<div class="large-8 columns"><h3>Agreements and Apply</h3></div>'
                ),
                HTML(
                    '<div class="large-8 columns"><p>Applicants for OEGlobal Memberships must acknowledge agreement with the Memorandum of Association, Website Terms and Conditions,'
                    "and Children's Online Privacy Protection Act Compliance. The first two are a bit long to fit on this pilot form but match the terms listed at the bottom"
                    'of the organizational member application at <a href="https://members.oeglobal.org/application/">https://members.oeglobal.org/application/</a>.</p></div>'
                ),
                HTML(
                    '<div class="large-8 columns"><h4>Memorandum of Association</h4></div>'
                ),
                HTML('<div class="moa-text large-8 columns"></div>'),
                Field("moa_terms", required=True),
                HTML(
                    '<div class="large-8 columns"><h4>Website Terms and Conditions</h4></div>'
                ),
                HTML('<div class="terms-text large-8 columns"></div>'),
                Field("terms_of_use", required=True),
                HTML(
                    '<div class="large-8 columns"><h4>Children\'s Online '
                    "Privacy Protection Act Compliance</h4></div>"
                ),
                HTML('<div class="coppa-text large-8 columns"></div>'),
                Field("coppa", required=True),
                Field("captcha", required=True),
                HTML(
                    '<div class="large-8 columns"><p>Thank you for completing this application form which places you in the very first group of individual members of Open Education Global.</p></div>'
                ),
                css_class="row",
            ),
        )
        self.helper.layout.append(Submit("save", "save"))

    def clean(self):
        cleaned_data = super(IndividualMembershipApplicationModelForm, self).clean()
        cleaned_data["individual_member"] = True
        if "display_name" not in cleaned_data or not cleaned_data["display_name"]:
            cleaned_data["display_name"] = "%s %s" % (
                cleaned_data["first_name"],
                cleaned_data["last_name"],
            )
        return cleaned_data

    class Meta:
        model = MembershipApplication
        exclude = ("id",)


class MemberLoginForm(forms.Form):
    organization = forms.ChoiceField()
    email = forms.EmailField()

    def __init__(self, *args, **kwargs):
        super(MemberLoginForm, self).__init__(*args, **kwargs)
        self.fields["organization"].choices = [
            (x.id, x.display_name) for x in Organization.active.all()
        ]

    def clean(self):
        cleaned_data = super(MemberLoginForm, self).clean()

        email = cleaned_data.get("email", "").strip()
        org = cleaned_data.get("organization")

        if (
            org
            and email
            and not Organization.active.filter(pk=org, contact__email__iexact=email)
        ):
            raise forms.ValidationError(
                "E-mail you entered is not associated with selected organization."
                + "Please contact members services if you require assistance.",
                code="invalid-email",
            )

        return cleaned_data


class AddressModelForm(forms.ModelForm):
    class Meta:
        model = Address
        fields = (
            "street_address",
            "supplemental_address_1",
            "supplemental_address_2",
            "city",
            "postal_code",
            "state_province",
            "country",
            "address_type",
        )


class BillingLogForm(forms.ModelForm):
    log_type = forms.ChoiceField(
        widget=forms.RadioSelect,
        choices=(
            ("create_general_note", "Add a General note"),
            ("create_note", "Add an Accounting note"),
        ),
        label="Action",
    )

    def __init__(self, *args, **kwargs):
        super(BillingLogForm, self).__init__(*args, **kwargs)

        self.fields["organization"].widget = forms.HiddenInput()
        self.fields["user"].widget = forms.HiddenInput()

        self.helper = FormHelper(self)

        self.helper.layout = Layout(
            Div(Field("log_type", ng_model="logtype"), css_class="row"),
            Div(
                HTML("<p>Note will be visible to other staff members</p>"),
                Field("note"),
                ng_show="logtype === 'create_note' || logtype === 'create_general_note",
            ),
            Field("organization"),
            Field("user"),
            Submit("submit", "submit"),
        )

    class Meta:
        model = BillingLog
        fields = ("log_type", "organization", "user", "note")


class ReportedStatisticModelForm(forms.ModelForm):
    report_date = forms.ChoiceField(label="Reported period, until:")

    def __init__(self, *args, **kwargs):
        self.obj = kwargs.get("instance", None)
        if self.obj:
            self.organization = self.obj.organization
        else:
            self.organization = kwargs.pop("organization")

        super(ReportedStatisticModelForm, self).__init__(*args, **kwargs)

        base = datetime.datetime(2016, 3, 1, 0, 0, 0)
        self.fields["report_date"].choices = [
            (i.strftime("%Y-%m-%d"), i.strftime("%B %Y"))
            for i in [base - relativedelta(months=x * 3) for x in range(0, 20)]
        ]
        if not self.obj:
            try:
                previous_statistic = ReportedStatistic.objects.filter(
                    organization=self.organization
                ).latest("report_date")
            except ReportedStatistic.DoesNotExist:
                previous_statistic = None

            if previous_statistic:
                for item in [
                    "site_visits",
                    "orig_courses",
                    "trans_courses",
                    "orig_course_lang",
                    "trans_course_lang",
                    "oer_resources",
                    "trans_oer_resources",
                ]:
                    self.fields[item].initial = getattr(previous_statistic, item)

    def clean(self):
        cleaned_data = super(ReportedStatisticModelForm, self).clean()

        if (
            not self.obj
            and ReportedStatistic.objects.filter(
                organization=self.organization, report_date=cleaned_data["report_date"]
            ).exists()
        ):
            raise forms.ValidationError(
                "Reported statistic for this interval already exists. Please edit previous entry."
            )

        return cleaned_data

    def save(self, commit=True):
        instance = super(ReportedStatisticModelForm, self).save(commit=False)
        instance.organization = self.organization

        if commit:
            instance.save()
        return instance

    class Meta:
        model = ReportedStatistic
        fields = (
            "site_visits",
            "orig_courses",
            "trans_courses",
            "orig_course_lang",
            "trans_course_lang",
            "oer_resources",
            "trans_oer_resources",
            "comment",
            "report_date",
        )
