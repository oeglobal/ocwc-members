from django.urls import path

from .views import (
    StaffIndex,
    OrganizationStaffListView,
    OrganizationStaffDetailView,
    InvoiceStaffView,
    InvoicePhantomJSView,
    BillingLogCreateView,
    OrganizationBillingLogListingView,
    OrganizationExportExcel,
    OrganizationStaffNoContactListView,
    OrganizationStaffCccOerListView,
    OrganizationExportCccoerExcel,
    QuickBooksLogin,
)

app_name = "staff"
urlpatterns = [
    path("", StaffIndex.as_view(), name="index"),
    path("invoice/view/<int:pk>/", InvoiceStaffView.as_view(), name="invoice-view"),
    path(
        "invoice/key/<str:access_key>/",
        InvoicePhantomJSView.as_view(lookup_field="access_key"),
        name="invoice-phantomjs-view",
    ),
    # path("invoice/create/", InvoiceCreateView.as_view(), name="invoice-create"),
    path(
        "billinglog/create/",
        BillingLogCreateView.as_view(),
        name="billinglog-create",
    ),
    path(
        "billinglog/list/<str:username>/",
        OrganizationBillingLogListingView.as_view(),
        name="billinglog-username-listing",
    ),
    path(
        "organization/view/<int:pk>/",
        OrganizationStaffDetailView.as_view(),
        name="organization-view",
    ),
    path(
        "organization/list/excel/",
        OrganizationExportExcel.as_view(),
        name="organization-list-excel",
    ),
    path(
        "organization/list/excel-cccoer/",
        OrganizationExportCccoerExcel.as_view(),
        name="organization-list-excel-cccoer",
    ),
    path(
        "organization/list/nocontact/",
        OrganizationStaffNoContactListView.as_view(),
        name="organization-list-nocontact",
    ),
    path(
        "organization/list/cccoer/",
        OrganizationStaffCccOerListView.as_view(),
        name="organization-list-cccoer",
    ),
    path(
        "organization/list/",
        OrganizationStaffListView.as_view(),
        name="organization-list",
    ),
    path("quickbooks/", QuickBooksLogin.as_view(), name="quickbooks"),
]
