from django.urls import path

from .views import OrganizationIndex, OrganizationDetailView, OrganizationEdit, \
    ReportedStatisticDetailView, ReportedStatisticEditView, ReportedStatisticAddView, \
    AddressEditView

from vanilla import TemplateView

app_name = 'crm'
urlpatterns = [
    path('member/view/<int:pk>/', OrganizationDetailView.as_view(), name='organization-view'),
    path('member/edit/<int:pk>/', OrganizationEdit.as_view(), name='organization-edit'),
    path('member/address/edit/<int:pk>/', AddressEditView.as_view(), name='address-edit'),
    path('member/statistics/view/<int:pk>/', ReportedStatisticDetailView.as_view(),
        name='reported-statistics-view'),
    path('member/statistics/edit/<int:pk>/', ReportedStatisticEditView.as_view(), name='reported-statistics-edit'),
    path('member/statistics/add/<int:pk>/', ReportedStatisticAddView.as_view(), name='reported-statistics-add'),
    path('', OrganizationIndex.as_view(), name='index'),
]
