import os, sys


def next_to_root(*additional_paths):
    return os.path.realpath(
        os.path.join(
            os.path.dirname(os.path.abspath(__file__)), "..", "..", *additional_paths
        )
    )


ADMINS = (
    # ('Jan Gondol', 'jan@jangondol.com'),
)

MANAGERS = ADMINS
TESTING = sys.argv[1:2] == ["test"]

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Default primary key field type
# https://docs.djangoproject.com/en/4.2/ref/settings/#default-auto-field
DEFAULT_AUTO_FIELD = "django.db.models.AutoField"  # Keep existing behavior

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/4.2/ref/settings/#allowed-hosts
ALLOWED_HOSTS = ["members.ocwonsortium.org", "members.oeglobal.org"]

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = "America/Chicago"

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = "en-us"

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = False

# Password Hasher Settings
PASSWORD_HASHERS = [
    "django.contrib.auth.hashers.PBKDF2PasswordHasher",
    "django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher",
    "django.contrib.auth.hashers.Argon2PasswordHasher",
    "django.contrib.auth.hashers.BCryptSHA256PasswordHasher",
]

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"
MEDIA_ROOT = next_to_root("media/")
MEDIA_URL = "/media/"

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/var/www/example.com/static/"
STATIC_ROOT = next_to_root("static/")

# URL prefix for static files.
# Example: "http://example.com/static/", "http://static.example.com/"
STATIC_URL = "/static/"

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
)

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

ROOT_URLCONF = "members.urls"

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = "members.wsgi.application"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.contrib.auth.context_processors.auth",
                "django.template.context_processors.debug",
                "django.template.context_processors.i18n",
                "django.template.context_processors.media",
                "django.template.context_processors.static",
                "django.template.context_processors.tz",
                "django.template.context_processors.request",
                "django.contrib.messages.context_processors.messages",
                "crm.context_processors.invoice_years",
            ]
        },
    }
]

INSTALLED_APPS = (
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.sites",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.flatpages",
    "django.contrib.admin",
    "django.contrib.admindocs",
    "gunicorn",
    "tinymce",
    "rest_framework",
    # "floppyforms",
    "crispy_forms",
    "crispy_bootstrap4",
    "captcha",
    "crm",
    "elections",
    "conferences",
)

SESSION_SERIALIZER = "django.contrib.sessions.serializers.JSONSerializer"

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "filters": {"require_debug_false": {"()": "django.utils.log.RequireDebugFalse"}},
    "handlers": {
        "mail_admins": {
            "level": "ERROR",
            "filters": ["require_debug_false"],
            "class": "django.utils.log.AdminEmailHandler",
        }
    },
    "loggers": {
        "django.request": {
            "handlers": ["mail_admins"],
            "level": "ERROR",
            "propagate": True,
        }
    },
}

RAVEN_CONFIG = {}

LOGIN_URL = "/admin/"

CRISPY_TEMPLATE_PACK = "bootstrap4"
CRISPY_ALLOWED_TEMPLATE_PACKS = "bootstrap4"

# Run migrations when you change these two settings!
PREVIOUS_INVOICE_YEAR = "2024"
DEFAULT_INVOICE_YEAR = "2025"
NEXT_INVOICE_YEAR = "2026"

# Invoice settings
INVOICE_YEAR = DEFAULT_INVOICE_YEAR  # Use the same year as DEFAULT_INVOICE_YEAR
INVOICE_DESCRIPTION = f"Membership from January 1, {INVOICE_YEAR} - December 31, {INVOICE_YEAR}"
INVOICE_PAYMENT_MESSAGE = "Thank you for your business. We take credit card, ACH, wire and check payments. Please indicate invoice # that you pay so we can use it to apply payment."
INVOICE_PAYMENT_DETAILS = """OEG electronic payment information:
Bank Name: Bank of America
Bank account number: 4660 0261 2894
Account Type: Checking
Bank Transit Routing: 011000138
Fedwire ABA Number: 026009593
Swift: BOFAUS3N
Reference: Invoice Number"""
INVOICE_CC_EMAILS = "billing@oeglobal.org, memberservices@oeglobal.org"

ALLOWED_HOSTS = ["localhost", "members.oeglobal.org", "members.test"]

REST_FRAMEWORK = {
    "DEFAULT_RENDERER_CLASSES": (
        "rest_framework.renderers.JSONRenderer",
        "rest_framework.renderers.BrowsableAPIRenderer",
        "rest_framework_jsonp.renderers.JSONPRenderer",
    )
}

QB_BASE_SANDBOX = "https://sandbox-quickbooks.api.intuit.com"
QB_BASE_PROD = "https://quickbooks.api.intuit.com"

QB_ACTIVE = False

# noinspection PyUnresolvedReferences
from .localsettings import *

# Storage settings
STORAGES = {
    "default": {
        "BACKEND": "django.core.files.storage.FileSystemStorage",
    },
    "staticfiles": {
        "BACKEND": "django.contrib.staticfiles.storage.StaticFilesStorage",
    },
}

# TinyMCE Configuration
TINYMCE_DEFAULT_CONFIG = {
    "height": 360,
    "width": 800,
    "cleanup_on_startup": True,
    "custom_undo_redo_levels": 20,
    "selector": "textarea",
    "theme": "silver",
    "plugins": """
        textcolor save link image media preview codesample contextmenu
        table code lists fullscreen insertdatetime nonbreaking
        contextmenu directionality searchreplace wordcount visualblocks
        visualchars code fullscreen autolink lists charmap print hr
        anchor pagebreak
        """,
    "toolbar1": """
        fullscreen preview bold italic underline | fontselect,
        fontsizeselect | forecolor backcolor | alignleft alignright |
        aligncenter alignjustify | indent outdent | bullist numlist table |
        | link image media | codesample |
        """,
    "toolbar2": """
        visualblocks visualchars |
        charmap hr pagebreak nonbreaking anchor | code |
        """,
    "contextmenu": "formats | link image",
    "menubar": True,
    "statusbar": True,
}
