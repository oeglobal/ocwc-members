from .settings import *

# Test-specific database configuration
DATABASES['default']['TEST'] = {
    'NAME': 'members',  # Use the same database
    'SERIALIZE': False,  # Don't try to serialize data
    'CREATE_DB': False,  # Don't try to create a new database
    'CREATE_USER': False,  # Don't try to create a new user
    'MIRROR': 'default',  # Use the same database as default
    'DEPENDENCIES': []  # No dependencies
}

# Use test_ prefix for all tables in test mode
DATABASE_TABLE_PREFIX = 'test_'

# Use in-memory storage for testing
STORAGES = {
    "default": {
        "BACKEND": "django.core.files.storage.InMemoryStorage",
    },
    "staticfiles": {
        "BACKEND": "django.core.files.storage.InMemoryStorage",
    },
}

# Use in-memory cache for testing
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
    }
}

# Disable expensive services during testing
QB_ACTIVE = False

# Speed up password hashing
PASSWORD_HASHERS = [
    'django.contrib.auth.hashers.MD5PasswordHasher',
]

# Use local memory email backend for testing
EMAIL_BACKEND = 'django.core.mail.backends.locmem.EmailBackend'

# Disable Sentry for tests
SENTRY_DSN = None
