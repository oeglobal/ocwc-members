from django.conf import settings
from django.conf.urls import include
from django.urls import path
from django.conf.urls.static import static

from django.contrib import admin
from django.views.static import serve
from crm.views import IndexView, LoginKeyCheckView

admin.autodiscover()

urlpatterns = [
    path("accounts/", include("django.contrib.auth.urls")),
    path("admin/", admin.site.urls),
    path("pages/", include("django.contrib.flatpages.urls")),
    path("api/v1/", include("crm.urls_api")),
    path("crm/", include("crm.urls")),
    path("staff/", include("crm.urls_staff")),
    path("application/", include("crm.urls_application")),
    path(
        "login/<str:key>/",
        LoginKeyCheckView.as_view(),
        name="login-key-check",
    ),
    path("conferences/", include("conferences.urls")),
    path("elections/", include("elections.urls")),
    path("tinymce/", include("tinymce.urls")),
    path("", IndexView.as_view(), name="crm_index"),
]

if settings.DEBUG:
    urlpatterns += [
        path(
            "media/logos/<path:path>",
            serve,
            {"document_root": settings.MEDIA_ROOT + '/logos/'},
        ),
    ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
