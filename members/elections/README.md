# OEG Board Elections HOWTO

Short manual regarding election process:

1. OEG staff: https://members.oeglobal.org/admin/elections/election/

     * create election:

       * Title: 2022 Board Elections
       * Nominate until: date + time
       * Vote from: date + time
       * Vote until: date + time

     * after creation, OEG members are able to submit nominations (see next
       step)

2. OEG staff: https://members.oeglobal.org/admin/elections/proposition/

     * optional: add 1 to 5 propositions
     * note: once created, developers needs to also perform changes to forms and
       views to match the amount of propositions created

3. OEG members: https://members.oeglobal.org/elections/candidate/nominate/

     * submit nomination(s)
     * works only within period between election creation (see previous step)
       and date+time defined in "Nominate until"

4. nominees: <edit URL received in email>

     * accept or reject nomination + provide additional details

5. OEG staff: `manage.py candidate_copy --candidate <candidate ID> --target-election <election ID>`

     * optional: copies a data about candidate with given ID (ID is tied to
       a particular candidate in particular elections) into elections with
       given ID
     * can speed-up data filling if information did not change much since
       last candidacy

6. OEG staff: `manage.py election_emails`

     * optional: updates mailgun with emails of orgs that didn't vote yet
     * can facilitate mass-mailing regarding election process
     * can be repeated

7. OEG staff: https://members.oeglobal.org/admin/elections/candidate/

     * at minimum a) adjust "vetted" status and b) set seat type to a
       candidate(s): "Institutional"
     * possibly also other adjustments

8. OEG staff: https://www.oeglobal.org/about-us/elections/

     * update content for 2023: candidate profiles, etc.

9. OEG staff: `manage.py election_mailing`

     * send emails with login keys to members that haven't voted yet
     * typically shortly before or shortly after date+time defined in
       "Vote from"

10. OEG members: <vote URL received in email>

     * vote for candidates and propositions
     * vote URL is sent for example by `manage.py election_mailing`

11. OEG staff: `manage.py elections_didntvote`

     * exports Excel with members that didn't vote yet
     * not needed, but can facilitate mailing regarding election process,
       status, etc.

12. OEG staff: `manage.py election_results`

     * displays election results in the console
     * when done during voting period, shows preliminary results (e.g. results
       may still change if members vote after)
     * when done after voting period, shows final results

13. OEG staff: https://www.oeglobal.org/about-us/elections/

     * update content for 2023: results
