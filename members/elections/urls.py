# -*- coding: utf-8 -*-
from django.urls import path

from .views import (
    CandidateAddView,
    CandidateEditView,
    CandidateView,
    ElectionListView,
    VoteView,
    VoteAddFormView,
    VoteResults,
)
from .api import ElectionCandidatesListAPIView

app_name = "elections"
urlpatterns = [
    path("candidate/nominate/", CandidateAddView.as_view(), name="candidate-add"),
    path(
        "candidate/edit/<str:key>/",
        CandidateEditView.as_view(lookup_field="edit_link_key", lookup_url_kwarg="key"),
        name="candidate-edit",
    ),
    path(
        "candidate/view/<str:key>/",
        CandidateView.as_view(lookup_field="view_link_key", lookup_url_kwarg="key"),
        name="candidate-view",
    ),
    path(
        "candidate/list/<str:key>/",
        ElectionListView.as_view(
            lookup_field="view_nominations_key", lookup_url_kwarg="key"
        ),
        name="candidate-list",
    ),
    path(
        "api/candidate/list/<str:key>/",
        ElectionCandidatesListAPIView.as_view(),
        name="api-candidate-list",
    ),
    path("vote/add/<int:pk>/", VoteAddFormView.as_view(), name="vote-add"),
    path("vote/<int:pk>/", VoteView.as_view(), name="vote-view"),
    path("vote/results/", VoteResults.as_view(), name="vote-results"),
]
