Dear OEG Member,

The election period for the Open Education Global (OEG) Board of Directors (https://www.oeglobal.org/about-us/elections/) has begun.

This year we are asking you to cast your ballot for 5 seats on the OEG Board of Directors. Links to the candidate profiles are available at: https://www.oeglobal.org/about-us/elections/

All voting takes place online.

Please login to your membership account at https://members.oeglobal.org
where you will see a link to the ballot if you or your organization is eligible to vote. Once the votes have been cast, this link will disappear and you will see confirmation of your votes. Each organization votes for 5 board seats. Please select your 5 preferred candidates from the list of nominees. OEG desires a strong, internationally diverse board of directors. OEGs by-laws provide for weighting of election results to ensure representation from different regions of the world.

The election period starts today and ends May 22. Results will be announced by May 29.

If you have any questions about your membership or voting status, please send an email to memberservices@oeglobal.org

For any technical issues, please send an email to tech@oeglobal.org

Thank you!

Kind regards,

Marcela Morales and Igor Lesko
On behalf of the Open Education Global Nominating Committee
