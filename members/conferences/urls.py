from django.urls import path

from .views import (
    ConferenceIndex,
    InvoicePDF,
    InvoicePreview,
    PingInvoices,
    ConnectEmail,
)

app_name = "conferences"
urlpatterns = [
    path("", ConferenceIndex.as_view(), name="index"),
    path("invoice/ping/", PingInvoices.as_view(), name="invoice_ping"),
    path("invoice/<int:pk>/", InvoicePDF.as_view(), name="invoice_download"),
    path(
        "invoice/<int:pk>/<str:access_key>/",
        InvoicePreview.as_view(),
        name="invoice_preview",
    ),
    path("connect/", ConnectEmail.as_view(), name="connect_email"),
]
