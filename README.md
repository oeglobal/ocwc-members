# Open Education Global CRM

Custom Django CRM for handling OE Global membership information.

MIT Licensed.

## Features

* Membership Application Form
* Organization information, addresses, contacts
* GeoJSON Api for map of OCW Members: https://www.oeglobal.org/members/
* QuickBooks Online Integration
  * OAuth2 authentication
  * Create draft invoices for membership dues
  * Sync organization data

## Requirements

* Python 3.10+
* Django 4.2 LTS
* PostgreSQL 12+
* QuickBooks Online API access (for invoice management)

## QuickBooks Integration

The system integrates with QuickBooks Online for invoice management. Key features:

### Configuration

Required settings in `settings.py`:
```python
# QuickBooks API Configuration
QB_CLIENT_ID = "your_client_id"
QB_CLIENT_SECRET = "your_client_secret"
QB_CALLBACK_URL = "your_callback_url"
QB_ENVIRONMENT = "sandbox" or "production"

# Invoice Settings
INVOICE_YEAR = "2025"  # Used for invoice numbering
INVOICE_DESCRIPTION = "Membership from January 1, 2025 - December 31, 2025"
INVOICE_PAYMENT_MESSAGE = "Thank you for your business..."
INVOICE_PAYMENT_DETAILS = """Payment details here..."""
INVOICE_CC_EMAILS = "billing@oeglobal.org, memberservices@oeglobal.org"
```

### Invoice Creation

Create draft invoices using the management command:
```bash
python manage.py create_invoice <member_id> <amount>
```

The command will:
- Create a draft invoice with number format: `{member_id}-{year}`
- Set invoice date and due date to current date
- Use billing contacts from the organization (falls back to lead contact)
- Include payment details and CC relevant staff emails
- Log the invoice creation in BillingLog

Example:
```bash
python manage.py create_invoice 826 500
```

### OAuth2 Authentication

Run the sync command to authenticate with QuickBooks:
```bash
python manage.py sync_qbo
```

This will:
1. Start OAuth2 flow if not authenticated
2. Open authorization URL in browser
3. Store tokens securely in Profile model

## Deployment

see [README in ansible subdirectory](ansible/README.md)

## Dev

Manually run:

`/Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome --headless --remote-debugging-port=9222 --disable-gpu`

on macOS to get headless Chrome.

Branches, pull-requests, releases, etc.: according to [git-flow](http://danielkummer.github.io/git-flow-cheatsheet/)


### Git hooks

For source code formatting, etc.:

`pre-commit install`


## Testing

The project uses pytest for testing. To run the tests:

1. Install dev dependencies:

```bash
poetry install --with dev
```

2. Run tests:

```bash
poetry run pytest
```

This will run all tests and generate a coverage report. The configuration in `pyproject.toml` enables:
- Verbose output (`-v`)
- Coverage reporting (`--cov`)
- Missing line reporting (`--cov-report=term-missing`)

### Writing Tests

Tests should be placed in the `tests` directory and follow these conventions:
- Test files should be named `test_*.py` or `*_test.py`
- Use pytest fixtures for test setup
- Use factory-boy for model factories
- Aim for high test coverage of business logic

## Contact

General contact: [tech@oeglobal.org](mailto:tech@oeglobal.org)
