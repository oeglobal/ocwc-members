import pytest
from django.contrib.auth.models import User
from crm.models import Organization, Country, Continent

@pytest.fixture
def continent():
    return Continent.objects.create(name="Europe")

@pytest.fixture
def country(continent):
    return Country.objects.create(
        name="Netherlands",
        iso_code="NL",
        developing=False,
        continent=continent
    )

@pytest.fixture
def user():
    return User.objects.create_user(
        username="testuser",
        email="test@example.com",
        password="testpass123"
    )

@pytest.fixture
def organization(user, country):
    return Organization.objects.create(
        legal_name="Test University",
        display_name="Test University",
        slug="test-university",
        user=user,
        membership_type=5,  # Institutional Members
        membership_status=2,  # Current
        billing_type="normal",
        main_website="https://test.edu",
        ocw_website="https://ocw.test.edu",
        description="Test University Description"
    )
