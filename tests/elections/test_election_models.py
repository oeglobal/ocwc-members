import pytest
from datetime import datetime, timedelta
from django.utils import timezone
from elections.models import Election, Candidate, CandidateBallot, Proposition, PropositionBallot
from crm.models import Organization
from tests.crm.factories import OrganizationFactory

@pytest.mark.django_db
class TestElection:
    def test_create_election(self):
        election = Election.objects.create(
            title="Board Election 2024",
            nominate_until=timezone.now() + timedelta(days=30),
            vote_from=timezone.now() + timedelta(days=31),
            vote_until=timezone.now() + timedelta(days=45)
        )
        assert election.pk is not None
        assert election.view_nominations_key
        assert election.edit_nominations_key
        assert str(election) == "Board Election 2024"

@pytest.mark.django_db
class TestCandidate:
    @pytest.fixture
    def election(self):
        return Election.objects.create(
            title="Test Election",
            nominate_until=timezone.now() + timedelta(days=30)
        )

    @pytest.fixture
    def organization(self):
        return OrganizationFactory()

    def test_create_candidate(self, election, organization):
        candidate = Candidate.objects.create(
            election=election,
            organization=organization,
            status="nominated",
            candidate_first_name="John",
            candidate_last_name="Doe",
            candidate_email="john@example.com",
            sponsor_first_name="Jane",
            sponsor_last_name="Smith",
            sponsor_email="jane@example.com",
            seat_type="institutional"
        )
        assert candidate.pk is not None
        assert candidate.edit_link_key
        assert candidate.view_link_key
        assert candidate.status == "nominated"

    def test_candidate_email_board(self, election, organization):
        candidate = Candidate.objects.create(
            election=election,
            organization=organization,
            status="nominated",
            candidate_first_name="John",
            candidate_last_name="Doe",
            candidate_email="john@example.com",
            sponsor_first_name="Jane",
            sponsor_last_name="Smith",
            sponsor_email="jane@example.com"
        )
        # Test email_board method - this should not raise any exceptions
        candidate.email_board()

@pytest.mark.django_db
class TestCandidateBallot:
    @pytest.fixture
    def election(self):
        return Election.objects.create(title="Test Election")

    @pytest.fixture
    def organization(self):
        return OrganizationFactory()

    @pytest.fixture
    def candidate(self, election, organization):
        return Candidate.objects.create(
            election=election,
            organization=organization,
            candidate_first_name="John",
            candidate_last_name="Doe",
            candidate_email="john@example.com",
            sponsor_first_name="Jane",
            sponsor_last_name="Smith",
            sponsor_email="jane@example.com"
        )

    def test_create_ballot(self, election, organization, candidate):
        ballot = CandidateBallot.objects.create(
            election=election,
            organization=organization,
            voter_name="Test Voter",
            seat_type="institutional"
        )
        ballot.votes.add(candidate)
        assert ballot.pk is not None
        assert ballot.votes.count() == 1
        assert ballot.votes.first() == candidate

@pytest.mark.django_db
class TestProposition:
    @pytest.fixture
    def election(self):
        return Election.objects.create(title="Test Election")

    def test_create_proposition(self, election):
        prop = Proposition.objects.create(
            election=election,
            title="Test Proposition",
            description="Test Description",
            published=True
        )
        assert prop.pk is not None
        assert str(prop) == "Test Proposition"

@pytest.mark.django_db
class TestPropositionBallot:
    @pytest.fixture
    def election(self):
        return Election.objects.create(title="Test Election")

    @pytest.fixture
    def proposition(self, election):
        return Proposition.objects.create(
            election=election,
            title="Test Proposition"
        )

    @pytest.fixture
    def organization(self):
        return OrganizationFactory()

    def test_create_proposition_ballot(self, election, proposition, organization):
        ballot = PropositionBallot.objects.create(
            election=election,
            proposition=proposition,
            organization=organization,
            voter_name="Test Voter",
            vote=True
        )
        assert ballot.pk is not None
        assert ballot.vote is True
