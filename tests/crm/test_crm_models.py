import pytest
from django.core.exceptions import ValidationError
from django.db import IntegrityError
from .factories import (
    OrganizationFactory, ContactFactory, MembershipApplicationFactory,
    InvoiceFactory, ProfileFactory, CountryFactory
)

@pytest.mark.django_db
class TestOrganization:
    def test_create_organization(self):
        org = OrganizationFactory()
        assert org.pk is not None
        assert org.display_name == org.legal_name
        assert org.slug
        assert org.membership_status == 2  # Current

    def test_organization_unique_slug(self):
        org1 = OrganizationFactory(slug="test-org")
        with pytest.raises(IntegrityError):
            OrganizationFactory(slug="test-org")

@pytest.mark.django_db
class TestContact:
    def test_create_contact(self):
        contact = ContactFactory()
        assert contact.pk is not None
        assert contact.organization is not None
        assert contact.email
        assert contact.contact_type == 6  # Lead Contact

    def test_contact_str(self):
        contact = ContactFactory(email="test@example.com")
        assert str(contact) == "test@example.com"

@pytest.mark.django_db
class TestMembershipApplication:
    def test_create_application(self):
        application = MembershipApplicationFactory()
        assert application.pk is not None
        assert application.simplified_membership_type == "institutional"
        assert application.institution_type == "higher-ed"
        assert application.display_name  # Just check it's not empty

    def test_application_with_country(self):
        country = CountryFactory()
        application = MembershipApplicationFactory(institution_country=country)
        assert application.institution_country == country

@pytest.mark.django_db
class TestInvoice:
    def test_create_invoice(self):
        invoice = InvoiceFactory()
        assert invoice.pk is not None
        assert invoice.invoice_type == "issued"
        assert invoice.amount > 0
        assert invoice.access_key  # Should be auto-generated

    def test_invoice_str(self):
        org = OrganizationFactory(display_name="Test Org")
        invoice = InvoiceFactory(organization=org, invoice_number="INV001")
        assert str(invoice) == "Invoice INV001 (Test Org)"

    def test_get_pdf_url(self):
        invoice = InvoiceFactory(pdf_filename="test.pdf")
        assert "test.pdf" in invoice.get_pdf_url()

@pytest.mark.django_db
class TestProfile:
    def test_create_profile(self):
        profile = ProfileFactory()
        assert profile.pk is not None
        assert profile.user is not None
        assert not profile.qb_valid

    def test_profile_str(self):
        profile = ProfileFactory()
        assert str(profile) == profile.user.username
