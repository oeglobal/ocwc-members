import factory
from django.contrib.auth.models import User
from crm.models import (
    Organization,
    Contact,
    MembershipApplication,
    Invoice,
    Profile,
    Country,
)
from faker import Faker
import datetime

fake = Faker()


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User
        skip_postgeneration_save = True

    username = factory.Sequence(lambda n: f"user{n}")
    email = factory.LazyAttribute(lambda obj: f"{obj.username}@example.com")
    password = factory.PostGenerationMethodCall("set_password", "password")


class CountryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Country

    name = factory.Sequence(lambda n: f"Country {n}")
    iso_code = factory.Sequence(lambda n: f"C{n}")
    developing = False


class OrganizationFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Organization

    legal_name = factory.Sequence(lambda n: f"Organization {n}")
    display_name = factory.LazyAttribute(lambda obj: obj.legal_name)
    slug = factory.Sequence(lambda n: f"org-{n}")
    membership_type = 5  # Institutional Members
    membership_status = 2  # Current
    billing_type = "normal"
    main_website = factory.LazyAttribute(lambda obj: f"https://{obj.slug}.edu")
    ocw_website = factory.LazyAttribute(lambda obj: f"https://ocw.{obj.slug}.edu")
    description = factory.LazyAttribute(lambda obj: fake.text(max_nb_chars=200))


class ContactFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Contact

    organization = factory.SubFactory(OrganizationFactory)
    first_name = factory.LazyAttribute(lambda obj: fake.first_name())
    last_name = factory.LazyAttribute(lambda obj: fake.last_name())
    email = factory.LazyAttribute(lambda obj: fake.email())
    contact_type = 6  # Lead Contact


class MembershipApplicationFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = MembershipApplication

    display_name = factory.Sequence(lambda n: f"Applicant Organization {n}")
    institution_type = "higher-ed"
    institution_country = factory.SubFactory(CountryFactory)
    org_affiliation = ""
    app_status = "Submitted"
    simplified_membership_type = "institutional"


class InvoiceFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Invoice

    organization = factory.SubFactory(OrganizationFactory)
    amount = factory.LazyAttribute(lambda obj: fake.random_int(min=100, max=1000))
    pub_date = factory.LazyFunction(datetime.datetime.now)
    invoice_type = "issued"
    invoice_number = factory.Sequence(lambda n: f"INV{n:03d}")
    access_key = factory.LazyAttribute(lambda obj: str(fake.uuid4())[:32])
    pdf_filename = factory.LazyAttribute(lambda obj: f"{obj.invoice_number}.pdf")


class ProfileFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Profile
        django_get_or_create = ("user",)  # Handle unique constraint

    user = factory.SubFactory(UserFactory)
    qb_access_token = factory.LazyAttribute(lambda obj: str(fake.uuid4())[:32])
    qb_refresh_token = factory.LazyAttribute(lambda obj: str(fake.uuid4())[:32])
    qb_valid = False
