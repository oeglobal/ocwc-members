import os
import sys
import django

# Add the project root and members directory to the Python path
project_root = os.path.dirname(os.path.abspath(__file__))
members_dir = os.path.join(project_root, 'members')
sys.path.insert(0, project_root)
sys.path.insert(0, members_dir)

# Configure Django settings
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'members.settings')

# Initialize Django
django.setup()
